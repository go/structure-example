package main

import (
	"fmt"

	"gitlab.forge.hefr.ch/go/structure-example/hello"
)

func main() {
	fmt.Println(hello.HelloString())
}
