package hello

// Hello returns a "Hello" string
func HelloString() string {
	return "Hello, world."
}
